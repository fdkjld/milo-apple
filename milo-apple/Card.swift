import UIKit

class Slice: UIView {
    override func draw(_ rect: CGRect) {
        
        let height = layer.frame.height
        let width = layer.frame.width

        let bezier = UIBezierPath()

        /*
        bezier.move(to: CGPoint(x: height / 2, y: 0))
        bezier.addLine(to: CGPoint(x: width, y: 0))
        bezier.addLine(to: CGPoint(x: width, y: height))
        bezier.addLine(to: CGPoint(x: height / 2, y: height))
        bezier.addLine(to: CGPoint(x: 0, y: height / 2))
        bezier.close()
        */
        
        bezier.move(to: CGPoint(x: 0, y: 0))
        bezier.addLine(to: CGPoint(x: width, y: 0))
        bezier.addLine(to: CGPoint(x: width, y: height))
        bezier.addLine(to: CGPoint(x: height / 2, y: height))
        bezier.close()

        let shape = CAShapeLayer()
        shape.path = bezier.cgPath
        layer.mask = shape
    }
}

class Card: UIViewController, UIScrollViewDelegate {
    struct Image: Codable {
        let element: String
        let event: String
        let image: String
        
        init(element: String, event: String, image: String) {
            self.element = element
            self.event = event
            self.image = image
       }
       
       init() {
            element = ""
            event = ""
            image = ""
       }
    }

    struct Images: Codable {
       
       var images: [Image]
       
       init() {
           self.images = Array<Image>()
       }
    }

    var feed = Images()
    
    func scrollViewDidEndDecelerating(_ scroll: UIScrollView) {
        let scroll = round(scroll.contentOffset.x / scroll.frame.size.width)
        page.currentPage = Int(scroll)
    }
    
    var event: View.Event = View.Event()
    
    let content = UIView()
    let bar = UIView()
    let image1 = UIImageView()
    var page = UIPageControl()
    var priority = 100
    
    override var prefersStatusBarHidden: Bool{return true}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scrol = UIScrollView()
        scrol.translatesAutoresizingMaskIntoConstraints = false
        scrol.backgroundColor = UIColor(red: 0.05, green: 0.05, blue: 0.05, alpha: 1.0)
        /*scrol.bounces = false*/
        /*scrol.indicatorStyle = .black*/ 
        view.addSubview(scrol)
        
        NSLayoutConstraint.activate([
            scrol.topAnchor.constraint      (equalTo: view.topAnchor),
            scrol.bottomAnchor.constraint   (equalTo: view.bottomAnchor),
            scrol.leadingAnchor.constraint  (equalTo: view.leadingAnchor),
            scrol.trailingAnchor.constraint (equalTo: view.trailingAnchor),
            scrol.centerXAnchor.constraint  (equalTo: view.centerXAnchor),
            scrol.centerYAnchor.constraint  (equalTo: view.centerYAnchor),
        ])
        
        content.translatesAutoresizingMaskIntoConstraints = false
        content.backgroundColor = UIColor(red: 0.05, green: 0.05, blue: 0.05, alpha: 1.0)
        
        scrol.addSubview(content)
        
        NSLayoutConstraint.activate([
            content.heightAnchor.constraint     (greaterThanOrEqualTo:  scrol.heightAnchor),
            content.topAnchor.constraint        (equalTo:               scrol.topAnchor),
            content.bottomAnchor.constraint     (equalTo:               scrol.bottomAnchor),
            content.leadingAnchor.constraint    (equalTo:               scrol.leadingAnchor),
            content.trailingAnchor.constraint   (equalTo:               scrol.trailingAnchor),
            content.centerXAnchor.constraint    (equalTo:               scrol.centerXAnchor),
        ])
        
        let carusel = UIScrollView()
        carusel.translatesAutoresizingMaskIntoConstraints = false
        carusel.showsHorizontalScrollIndicator = false
        carusel.isPagingEnabled = true
        carusel.bounces = false
        carusel.delegate = self
        content.addSubview(carusel)

        NSLayoutConstraint.activate([
            carusel.heightAnchor.constraint     (equalTo: content.widthAnchor),
            carusel.widthAnchor.constraint      (equalTo: content.widthAnchor),
            carusel.topAnchor.constraint        (equalTo: content.topAnchor),
            carusel.centerXAnchor.constraint    (equalTo: content.centerXAnchor),
            carusel.leadingAnchor.constraint    (equalTo: content.leadingAnchor),
            carusel.trailingAnchor.constraint   (equalTo: content.trailingAnchor)
        ])
        
        bar.translatesAutoresizingMaskIntoConstraints = false
        carusel.addSubview(bar)

        NSLayoutConstraint.activate([
            bar.topAnchor.constraint        (equalTo: carusel.topAnchor),
            bar.bottomAnchor.constraint     (equalTo: carusel.bottomAnchor),
            bar.leadingAnchor.constraint    (equalTo: carusel.leadingAnchor),
            bar.trailingAnchor.constraint   (equalTo: carusel.trailingAnchor),
            bar.centerYAnchor.constraint    (equalTo: carusel.centerYAnchor)
        ])

        image1.translatesAutoresizingMaskIntoConstraints = false
        image1.contentMode = .scaleAspectFit

        DispatchQueue.global().async {
            let patch = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(self.event.intro).path
            DispatchQueue.main.sync {
                if FileManager.default.fileExists(atPath: patch) {
                    self.image1.image = UIImage(contentsOfFile: patch)
                }
            }
        }

        bar.addSubview(image1)

        let constraint = image1.trailingAnchor.constraint(equalTo: bar.trailingAnchor)
        constraint.priority = UILayoutPriority(Float(priority))
        priority += 1
        
        NSLayoutConstraint.activate([
            image1.widthAnchor.constraint    (equalTo: content.widthAnchor),
            image1.topAnchor.constraint      (equalTo: bar.topAnchor),
            image1.bottomAnchor.constraint   (equalTo: bar.bottomAnchor),
            image1.leadingAnchor.constraint  (equalTo: bar.leadingAnchor),
            constraint
        ])
        
        page = UIPageControl()
        page.translatesAutoresizingMaskIntoConstraints = false
        page.isUserInteractionEnabled = false
        page.numberOfPages = 1
        page.pageIndicatorTintColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        page.currentPageIndicatorTintColor = .white
        page.isHidden = true
        
        content.addSubview(page)
        
        NSLayoutConstraint.activate([
            page.topAnchor.constraint       (equalTo: carusel.bottomAnchor, constant: 5),
            page.leadingAnchor.constraint   (equalTo: carusel.leadingAnchor),
            page.trailingAnchor.constraint  (equalTo: carusel.trailingAnchor)
        ])
        
        if NSString(string: event.small).integerValue == 1 {
            let slice = Slice()
            slice.translatesAutoresizingMaskIntoConstraints = false
            slice.backgroundColor = .systemRed
            content.addSubview(slice)
            
            NSLayoutConstraint.activate([
                slice.heightAnchor.constraint    (equalToConstant:   25),
                slice.widthAnchor.constraint     (equalToConstant:   110),
                slice.trailingAnchor.constraint  (equalTo:           carusel.trailingAnchor,   constant: 0),
                slice.bottomAnchor.constraint    (equalTo:           carusel.bottomAnchor,     constant: 12.5)
            ])
            
            let hall = UILabel()
            hall.translatesAutoresizingMaskIntoConstraints = false
            hall.textColor = .white
            hall.font = hall.font.withSize(12.5)
            hall.text = "малый зал"
            hall.textAlignment = .center
            
            slice.addSubview(hall)
            
            NSLayoutConstraint.activate([
                hall.centerXAnchor.constraint(equalTo: slice.centerXAnchor),
                hall.centerYAnchor.constraint(equalTo: slice.centerYAnchor)
            ])
        }
        
        let name = UILabel()
        name.translatesAutoresizingMaskIntoConstraints = false
        name.textColor = .white
        name.font = name.font.withSize(25)
        name.adjustsFontSizeToFitWidth = true
        name.text = event.name
        content.addSubview(name)
        
        NSLayoutConstraint.activate([
            name.topAnchor.constraint       (equalTo: page.bottomAnchor),
            name.leadingAnchor.constraint   (equalTo: content.leadingAnchor,    constant: 20),
            name.trailingAnchor.constraint  (equalTo: content.trailingAnchor,   constant: -20)
        ])
        
        let description1 = UILabel()
        description1.textColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        description1.font = name.font.withSize(12.5)
        description1.adjustsFontSizeToFitWidth = true
        description1.translatesAutoresizingMaskIntoConstraints = false
        description1.text = event.description1
        content.addSubview(description1)
        
        NSLayoutConstraint.activate([
            description1.topAnchor.constraint       (equalTo: name.bottomAnchor,      constant: 10),
            description1.leadingAnchor.constraint   (equalTo: content.leadingAnchor,  constant: 20),
            description1.trailingAnchor.constraint  (equalTo: content.trailingAnchor, constant: -20)
        ])
        
        let description2 = UILabel()
        description2.translatesAutoresizingMaskIntoConstraints = false
        description2.textColor = .white
        description2.font = name.font.withSize(10)
        description2.numberOfLines = 20
        description2.text = event.description2
        content.addSubview(description2)
        
        NSLayoutConstraint.activate([
            description2.topAnchor.constraint       (equalTo: description1.bottomAnchor,  constant: 20),
            description2.leadingAnchor.constraint   (equalTo: content.leadingAnchor,      constant: 20),
            description2.trailingAnchor.constraint  (equalTo: content.trailingAnchor,     constant: -20)
        ])
        
        let date = UILabel()
        date.translatesAutoresizingMaskIntoConstraints = false
        date.textColor = .white
        date.font = UIFont(name: "Avenir-Light", size: 25)
        date.textAlignment = .right
        
        let iso = "\(event.date) \(event.time)"
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        if let d = formatter.date(from: iso) {
            let calendar = Calendar.current
            let keys = ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"]
            date.text = "\(calendar.component(.day, from: d)) \(keys[calendar.component(.month, from: d)-1]), в \(calendar.component(.hour, from: d)):\(String(format: "%02d", calendar.component(.minute, from: d)))"
        }
        
        content.addSubview(date)
        
        NSLayoutConstraint.activate([
            date.topAnchor.constraint       (equalTo: description2.bottomAnchor,    constant: 30),
            date.leadingAnchor.constraint   (equalTo: content.leadingAnchor,        constant: 20),
            date.trailingAnchor.constraint  (equalTo: content.trailingAnchor,       constant: -20)
        ])
        
        let price = UILabel()
        price.translatesAutoresizingMaskIntoConstraints = false
        price.textColor = .white
        price.font = name.font.withSize(15)
        price.text = "билет от \(event.price) руб."
        price.textAlignment = .right
        content.addSubview(price)
        
        NSLayoutConstraint.activate([
            price.topAnchor.constraint      (equalTo: date.bottomAnchor,        constant: 10),
            price.leadingAnchor.constraint  (equalTo: content.leadingAnchor,    constant: 20),
            price.trailingAnchor.constraint (equalTo: content.trailingAnchor,   constant: -20)
        ])
        
        let stack = UIStackView()
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.spacing = 1
        stack.distribution = .fillEqually
        content.addSubview(stack)
        
        NSLayoutConstraint.activate([
            stack.heightAnchor.constraint       (equalToConstant:       50),
            stack.topAnchor.constraint          (greaterThanOrEqualTo:  price.bottomAnchor, constant: 40),
            stack.leadingAnchor.constraint      (equalTo:               content.leadingAnchor),
            stack.trailingAnchor.constraint     (equalTo:               content.trailingAnchor)
        ])
        
        let phone = UIButton(type: .system)
        
        if let font: UILabel = phone.titleLabel {
            font.font = font.font.withSize(12.5)
        }
        
        phone.translatesAutoresizingMaskIntoConstraints = false
        phone.setTitle("позвонить", for: .normal)
        phone.tintColor = .white
        phone.backgroundColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.0)
        phone.addTarget(self, action: #selector(call), for: .touchUpInside)
        stack.addArrangedSubview(phone)
        
        /*
        let site = UIButton(type: .system)
        
        if let font: UILabel = site.titleLabel {
            font.font = font.font.withSize(12.5)
        }
        
        site.translatesAutoresizingMaskIntoConstraints = false
        site.setTitle("сайт", for: .normal)
        site.tintColor = .white
        site.backgroundColor = UIColor(red: 0.075, green: 0.075, blue: 0.075, alpha: 1.0)
        site.addTarget(self, action: #selector(miloconcerthall), for: .touchUpInside)
        stack.addArrangedSubview(site)
         */
 
        let buy = UIButton(type: .system)
        
        if let font: UILabel = buy.titleLabel {
            font.font = font.font.withSize(12.5)
        }
        
        buy.translatesAutoresizingMaskIntoConstraints = false
        buy.setTitle("купить", for: .normal)
        buy.tintColor = .white
        buy.backgroundColor = UIColor(red: 0.09, green: 0.09, blue: 0.09, alpha: 1.0)
        buy.addTarget(self, action: #selector(open), for: .touchUpInside)
        stack.addArrangedSubview(buy)
        
        if #available(iOS 13.0, *) {
            NSLayoutConstraint.activate([
                stack.bottomAnchor.constraint(equalTo:  content.bottomAnchor, constant: -30)
            ])
        } else {
            let close = UIButton(type: .system)

            if let font: UILabel = close.titleLabel {
                font.font = font.font.withSize(15)
            }

            close.translatesAutoresizingMaskIntoConstraints = false
            close.setTitle("закрыть", for: .normal)
            close.tintColor = .white
            close.backgroundColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.0)
            close.addTarget(self, action: #selector(button), for: .touchUpInside)
            content.addSubview(close)

            NSLayoutConstraint.activate([
                close.heightAnchor.constraint    (equalToConstant:  55),
                close.topAnchor.constraint       (equalTo:          stack.bottomAnchor, constant: 1),
                close.bottomAnchor.constraint    (equalTo:          content.bottomAnchor),
                close.leadingAnchor.constraint   (equalTo:          content.leadingAnchor),
                close.trailingAnchor.constraint  (equalTo:          content.trailingAnchor)
            ])
        }
        
        load();
    }
    
    func inflate(image: String) {
        let size = feed.images.count
        
        if size > 0 {
            page.isHidden = false
            page.numberOfPages = size
            page.numberOfPages += 1
        }
        
        let image2 = UIImageView()
        image2.translatesAutoresizingMaskIntoConstraints = false
        image2.contentMode = .scaleAspectFit

        bar.addSubview(image2)
        
        let constraint1 = image2.trailingAnchor.constraint(equalTo: bar.trailingAnchor)
        constraint1.priority = UILayoutPriority(Float(priority))
        priority += 1

        NSLayoutConstraint.activate([
            image2.widthAnchor.constraint   (equalTo: self.content.widthAnchor),
            image2.topAnchor.constraint     (equalTo: self.bar.topAnchor),
            image2.bottomAnchor.constraint  (equalTo: self.bar.bottomAnchor),
            image2.leadingAnchor.constraint (equalTo: self.image1.trailingAnchor),
            constraint1
        ])
        
        DispatchQueue.global().async {
            let patch = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(image).path
            if FileManager.default.fileExists(atPath: patch) {
                let load = UIImage(contentsOfFile: patch)
                DispatchQueue.main.sync {
                    image2.image = load
                }
            }
        }
    }
    
    @objc func load() {
        DispatchQueue(label: "ru.miloconcerthall.label").async {
            do {
                let json = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("images.json")
                
                let url2 = URL(string: "http://miloconcerthall.ru/app/json/images.php?event=\(self.event.element)")
                if try Data(contentsOf: url2!).count == 0 {return}
                try Data(contentsOf: url2!).write(to: json)
                
                
                self.feed = try JSONDecoder().decode(Images.self, from: Data(contentsOf: json))
                let size = self.feed.images.count
                
                for a in 0..<size {
                    let file = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(self.feed.images[a].image)
                    
                    if FileManager.default.fileExists(atPath: file.path) {
                        print("exist \(self.feed.images[a].image)")
                        
                        DispatchQueue.main.async {
                            self.inflate(image: self.feed.images[a].image)
                        }
                    } else {
                        if let url3 = URL(string: "http://miloconcerthall.ru/app/\(self.feed.images[a].image)") {
                            print("load \(self.feed.images[a].image)")
                            let image = UIImage(data: try Data(contentsOf: url3))
                            try image?.pngData()?.write(to: file)
                            
                            DispatchQueue.main.async {
                                self.inflate(image: self.feed.images[a].image)
                            }
                        }
                    }
                }
                
            } catch let error {
                print(error)
            }
        }
    }
    
    @objc func button(sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    @objc func call(sender: UIButton) {
        if let url = URL(string: "tel://+78312138686"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @objc func open(sender: UIButton) {
        if let url = URL(string: event.url), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
}
