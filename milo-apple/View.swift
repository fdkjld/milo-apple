import UIKit

class Custom: UIView {
    var event: View.Event = View.Event()
    
    override func draw(_ rect: CGRect) {
        
        let height = layer.frame.height
        let width = layer.frame.width

        let bezier = UIBezierPath()

        bezier.move(to: CGPoint(x: 0, y: 0))
        bezier.addLine(to: CGPoint(x: width-10, y: 0))
        bezier.addLine(to: CGPoint(x: width, y: 10))
        bezier.addLine(to: CGPoint(x: width, y: height))
        bezier.addLine(to: CGPoint(x: 0, y: height))
        bezier.close()

        let shape = CAShapeLayer()
        shape.path = bezier.cgPath
        layer.mask = shape
    }
}

class View: UIViewController {
    
    /*Структура данных событий.*/
    
    struct Event: Codable {
        let description1: String
        let description2: String
        let element: String
        let small: String
        let intro: String
        let name: String
        let price: String
        let time: String
        let date: String
        let url: String
        
        init(description1: String, description2: String,element: String,small: String,intro: String,name: String,price: String,time: String,date: String, url: String) {
            self.description1 = description1
            self.description2 = description2
            self.element = element
            self.small = small
            self.intro = intro
            self.name = name
            self.price = price
            self.time = time
            self.date = date
            self.url = url
        }
        
        init() {
            description1 = ""
            description2 = ""
            element = ""
            small = ""
            intro = ""
            name = ""
            price = ""
            time = ""
            date = ""
            url = ""
        }
    }
    
    let refresh = UIRefreshControl()

    struct Events: Codable {
        
        var events: [Event]
        
        init() {
            self.events = Array<Event>()
        }
    }
    
    let carusel = UIScrollView()
    let scrol = UIScrollView()
    var lenta = UIView()
    let head = UIView()
    
    func notifications() {
        let notification = UNUserNotificationCenter.current()
        notification.requestAuthorization(options: [.alert, .sound, .badge]) {
            granted, error in print("permission granted: \(granted)")
        }
        
        /*Локальные уведомления.*/
        
        let content = UNMutableNotificationContent ()

        content.title = "LITLE BIG"
        content.body = "22 мая, чт. в 21 час., билет от 1500 рублей."
        content.sound = UNNotificationSound.default
        content.badge = 1
            
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 15, repeats: false)
        
        let identifier = "n001"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

        notification.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        notifications()
        
        DispatchQueue.global().async {
            print("name:\(UIDevice.current.name) IOS:\(UIDevice.current.systemVersion) UUID:\(UIDevice.current.identifierForVendor!.uuidString)")
            
            do {
                if let url = URL(string: "http://miloconcerthall.ru/app/info?uuid=\(UIDevice.current.identifierForVendor!.uuidString)&ios=\(UIDevice.current.systemVersion)") {
                    print("info \(try String(contentsOf: url))")
                }
            } catch _ {
                
            }
        }
        
        carusel.translatesAutoresizingMaskIntoConstraints = false
        carusel.isPagingEnabled = true
        carusel.showsHorizontalScrollIndicator = false
        carusel.bounces = false
        view.addSubview(carusel)

        /*Для шторки.*/
        
        if #available(iOS 11.0, *) {
            NSLayoutConstraint.activate([
                carusel.topAnchor.constraint        (equalTo: view.safeAreaLayoutGuide.topAnchor),
                carusel.bottomAnchor.constraint     (equalTo: view.safeAreaLayoutGuide.bottomAnchor),
                carusel.leadingAnchor.constraint    (equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                carusel.trailingAnchor.constraint   (equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                carusel.centerXAnchor.constraint    (equalTo: view.safeAreaLayoutGuide.centerXAnchor),
                carusel.centerYAnchor.constraint    (equalTo: view.safeAreaLayoutGuide.centerYAnchor)
            ])
        } else {
            NSLayoutConstraint.activate([
                carusel.topAnchor.constraint        (equalTo: view.topAnchor, constant: 20),
                carusel.bottomAnchor.constraint     (equalTo: view.bottomAnchor),
                carusel.leadingAnchor.constraint    (equalTo: view.leadingAnchor),
                carusel.trailingAnchor.constraint   (equalTo: view.trailingAnchor),
                carusel.centerXAnchor.constraint    (equalTo: view.centerXAnchor),
                carusel.centerYAnchor.constraint    (equalTo: view.centerYAnchor, constant: 10)
            ])
        }
        
        let container = UIView()
        container.translatesAutoresizingMaskIntoConstraints = false
        carusel.addSubview(container)
        
        NSLayoutConstraint.activate([
            container.topAnchor.constraint      (equalTo: carusel.topAnchor),
            container.bottomAnchor.constraint   (equalTo: carusel.bottomAnchor),
            container.centerYAnchor.constraint  (equalTo: carusel.centerYAnchor),
            container.leadingAnchor.constraint  (equalTo: carusel.leadingAnchor),
            container.trailingAnchor.constraint (equalTo: carusel.trailingAnchor)
        ])
        
        scrol.translatesAutoresizingMaskIntoConstraints = false
        scrol.alwaysBounceVertical = true
        container.addSubview(scrol)
        
        NSLayoutConstraint.activate([
            scrol.topAnchor.constraint      (equalTo: container.topAnchor),
            scrol.bottomAnchor.constraint   (equalTo: container.bottomAnchor),
            scrol.centerYAnchor.constraint  (equalTo: container.centerYAnchor),
            scrol.leadingAnchor.constraint  (equalTo: container.leadingAnchor),
            scrol.trailingAnchor.constraint (equalTo: container.trailingAnchor),/*Убрать когда будет дополнительная страница.*/
            scrol.widthAnchor.constraint    (equalTo: view.widthAnchor),
        ])

        refresh.addTarget(self, action: #selector(load), for: .valueChanged)
        refresh.backgroundColor = .black
        refresh.tintColor = .white
        scrol.addSubview(refresh)
        
        /*
        let contact1 = UIView()
        contact1.translatesAutoresizingMaskIntoConstraints = false
        container.addSubview(contact1)

        NSLayoutConstraint.activate([
            contact1.bottomAnchor.constraint     (equalTo: container.bottomAnchor),
            contact1.topAnchor.constraint        (equalTo: container.topAnchor),
            contact1.leadingAnchor.constraint    (equalTo: scrol.trailingAnchor),
            contact1.trailingAnchor.constraint   (equalTo: container.trailingAnchor),
            contact1.widthAnchor.constraint      (equalTo: view.widthAnchor)
        ])
        */
        
        /*Шапка.*/
        
        head.translatesAutoresizingMaskIntoConstraints = false
        head.backgroundColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.0)
        scrol.addSubview(head)
        
        NSLayoutConstraint.activate([
            head.heightAnchor.constraint    (equalToConstant:   180),
            head.widthAnchor.constraint     (equalTo:           scrol.widthAnchor),
            head.topAnchor.constraint       (equalTo:           scrol.topAnchor),
            head.bottomAnchor.constraint    (lessThanOrEqualTo: scrol.bottomAnchor)
        ])
        
        let logo = UIImageView()
        logo.translatesAutoresizingMaskIntoConstraints = false
        logo.contentMode = .scaleAspectFit
        logo.image = UIImage(named: "milo")
        head.addSubview(logo)
        
        NSLayoutConstraint.activate([
            logo.widthAnchor.constraint     (lessThanOrEqualTo: head.widthAnchor,       constant: -100),
            logo.centerYAnchor.constraint   (equalTo:           head.centerYAnchor),
            logo.leadingAnchor.constraint   (equalTo:           head.leadingAnchor,     constant: 15),
            logo.trailingAnchor.constraint  (lessThanOrEqualTo: head.trailingAnchor,    constant: 15)
        ])
        
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        label.font = label.font.withSize(15)
        label.text = "События"
        head.addSubview(label)
        
        NSLayoutConstraint.activate([
            label.bottomAnchor.constraint   (equalTo: head.bottomAnchor,  constant: -15),
            label.leadingAnchor.constraint  (equalTo: head.leadingAnchor, constant: 15)
        ])
        
        let border = UIView()
        border.translatesAutoresizingMaskIntoConstraints = false
        border.backgroundColor = .systemRed
        head.addSubview(border)
        
        NSLayoutConstraint.activate([
            border.heightAnchor.constraint  (equalToConstant:   2),
            border.widthAnchor.constraint   (equalTo:           label.widthAnchor, constant: 10),
            border.centerXAnchor.constraint (equalTo:           label.centerXAnchor),
            border.bottomAnchor.constraint  (equalTo:           head.bottomAnchor)
        ])
        
        /*
        let contact2 = UIButton(type: .system)
        
        if let font: UILabel = contact2.titleLabel {
            font.font = font.font.withSize(15)
        }
        
        contact2.translatesAutoresizingMaskIntoConstraints = false
        contact2.setTitle("Контакты", for: .normal)
        contact2.tintColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
        contact2.backgroundColor = UIColor(red: 0.075, green: 0.075, blue: 0.075, alpha: 1.0)
        contact2.addTarget(self, action: #selector(scroll1), for: .touchUpInside)
        head.addSubview(contact2)
        
        NSLayoutConstraint.activate([
            contact2.bottomAnchor.constraint   (equalTo: head.bottomAnchor,  constant: -10),
            contact2.centerYAnchor.constraint  (equalTo: label.centerYAnchor),
            contact2.leadingAnchor.constraint  (equalTo: label.trailingAnchor, constant: 15)
        ])
         */
        
        load()
    }
    
    var feed = Events()
    
    func content() {
        /*Контейнеры объектов событий.*/
        
        if self.lenta.subviews.count > 0 {
            self.lenta.removeFromSuperview()
            self.lenta = UIView()
        }
         
        self.lenta.translatesAutoresizingMaskIntoConstraints = false
        self.lenta.backgroundColor = UIColor(red: 0.07, green: 0.07, blue: 0.07, alpha: 1.0)
        self.scrol.addSubview(self.lenta)
    
        NSLayoutConstraint.activate([
            self.lenta.topAnchor.constraint         (equalTo: self.head.bottomAnchor),
            self.lenta.bottomAnchor.constraint      (equalTo: self.scrol.bottomAnchor),
            self.lenta.leadingAnchor.constraint     (equalTo: self.scrol.leadingAnchor),
            self.lenta.trailingAnchor.constraint    (equalTo: self.scrol.trailingAnchor),
            self.lenta.centerXAnchor.constraint     (equalTo: self.scrol.centerXAnchor)
        ])

        var last = Custom()

        let size = feed.events.count
        var a = 0
        func loop() {
            let event = Custom()
            event.event = feed.events[a]
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.open(_:)))
            event.addGestureRecognizer(tap)
            event.backgroundColor = UIColor(red: 0.09, green: 0.09, blue: 0.09, alpha: 1.0)
            event.translatesAutoresizingMaskIntoConstraints = false
            self.lenta.addSubview(event)
            
            NSLayoutConstraint.activate([
                event.heightAnchor.constraint       (equalToConstant:   150),
                event.leadingAnchor.constraint      (equalTo:           self.lenta.leadingAnchor,        constant: 15),
                event.trailingAnchor.constraint     (equalTo:           self.lenta.trailingAnchor,       constant: -15)
            ])
            
            if a == 0 {
                NSLayoutConstraint.activate([
                    event.topAnchor.constraint(equalTo: self.lenta.topAnchor, constant: 15)
                ])
            }
            
            if size == 1 {
                NSLayoutConstraint.activate([
                    event.bottomAnchor.constraint(equalTo: self.lenta.bottomAnchor, constant: -15)
                ])
            }
            
            if a > 0 && a < size-1 {
                NSLayoutConstraint.activate([event.topAnchor.constraint(equalTo: last.bottomAnchor, constant: 15)])
            }
            
            if a > 0 && a == size-1 {
                NSLayoutConstraint.activate([
                    event.topAnchor.constraint      (equalTo: last.bottomAnchor,        constant: 15),
                    event.bottomAnchor.constraint   (equalTo: self.lenta.bottomAnchor,  constant: -15)
                ])
            }
            
            let image = UIImageView()
            image.translatesAutoresizingMaskIntoConstraints = false
            image.contentMode = .scaleAspectFill
            image.clipsToBounds = true
            image.backgroundColor = UIColor(red: 0.19, green: 0.19, blue: 0.19, alpha: 1.0)
            
            event.addSubview(image)
            
            NSLayoutConstraint.activate([
                image.heightAnchor.constraint   (equalToConstant:   150),
                image.leadingAnchor.constraint  (equalTo:           event.leadingAnchor),
                image.trailingAnchor.constraint (equalTo:           event.centerXAnchor),
                image.centerYAnchor.constraint  (equalTo:           event.centerYAnchor)
            ])
            
            let title = UILabel()
            title.textColor = .white
            title.font = title.font.withSize(15)
            title.translatesAutoresizingMaskIntoConstraints = false
            title.text = feed.events[a].name
            title.adjustsFontSizeToFitWidth = true
            event.addSubview(title)
            
            NSLayoutConstraint.activate([
                title.topAnchor.constraint      (equalTo: event.topAnchor,         constant: 10),
                title.leadingAnchor.constraint  (equalTo: image.trailingAnchor,    constant: 10),
                title.trailingAnchor.constraint (equalTo: event.trailingAnchor,    constant: -10)
            ])
            
            let price = UILabel()
            price.textColor = .white
            price.font = price.font.withSize(12.5)
            price.translatesAutoresizingMaskIntoConstraints = false
            price.text = "от \(feed.events[a].price)"
            event.addSubview(price)
            
            NSLayoutConstraint.activate([
                price.topAnchor.constraint      (equalTo: title.bottomAnchor,   constant: 10),
                price.leadingAnchor.constraint  (equalTo: image.trailingAnchor, constant: 10)
            ])
            
            let description = UILabel()
            description.textColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 1.0)
            description.font = description.font.withSize(10)
            description.translatesAutoresizingMaskIntoConstraints = false
            title.adjustsFontSizeToFitWidth = true
            
            if NSString(string: feed.events[a].small).integerValue == 1 {
                description.text = "малый зал, \(feed.events[a].description1)"
            } else {
                description.text = "\(feed.events[a].description1)"
            }
            
            event.addSubview(description)
            
            NSLayoutConstraint.activate([
                description.topAnchor.constraint        (equalTo: price.bottomAnchor,   constant: 10),
                description.leadingAnchor.constraint    (equalTo: image.trailingAnchor, constant: 10),
                description.trailingAnchor.constraint   (equalTo: event.trailingAnchor, constant: -10)
            ])
            
            /*
            if NSString(string: feed.events[a].small).integerValue == 1 {
                let hall = UILabel()
                hall.translatesAutoresizingMaskIntoConstraints = false
                hall.backgroundColor = UIColor(red: 0.05, green: 0.05, blue: 0.05, alpha: 1.0)
                /*hall.transform = CGAffineTransform(rotationAngle: -CGFloat(Double.pi/2))*/
                hall.textColor = .white
                hall.font = hall.font.withSize(10)
                hall.text = "малый зал"
                hall.textAlignment = .center
                event.addSubview(hall)
                
                NSLayoutConstraint.activate([
                    hall.heightAnchor.constraint(equalToConstant: 20),
                    hall.widthAnchor.constraint(equalToConstant: 65),
                    hall.leadingAnchor.constraint(equalTo: event.leadingAnchor, constant: 0),
                    hall.bottomAnchor.constraint(equalTo: event.bottomAnchor, constant: 0)
                ])
            }
            */
            
            let day = UILabel()
            day.textColor = .white
            day.font = UIFont(name: "Avenir-Light", size: 22.5)
            day.translatesAutoresizingMaskIntoConstraints = false
            
            let iso = "\(feed.events[a].date) \(feed.events[a].time)"
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            if let d = formatter.date(from: iso) {
                let calendar = Calendar.current
                let keys = ["янв", "фев", "мар", "апр", "май", "июн", "июл", "авг", "сен", "окт", "ноя", "дек"]
                let weeks = ["пн", "вт", "ср", "чт", "пт", "сб", "вс"]
                
                day.text = "\(calendar.component(.day, from: d)) \(keys[calendar.component(.month, from: d)-1]), \(weeks[calendar.component(.weekday, from: d)-1])"
            }
            
            event.addSubview(day)
            
            NSLayoutConstraint.activate([
                day.bottomAnchor.constraint   (equalTo: event.bottomAnchor,     constant: -10),
                day.trailingAnchor.constraint (equalTo: event.trailingAnchor,   constant: -12.5)
            ])
            
            last = event
            
            DispatchQueue.global().async {
                let patch = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(self.feed.events[a].intro).path
                if FileManager.default.fileExists(atPath: patch) {
                    let load = UIImage(contentsOfFile: patch)
                    DispatchQueue.main.sync {
                        image.image = load
                        a += 1
                        if a < size {loop()}
                    }
                }
            }
        }
        
        if size > 0 {loop()}
    }
    
    @objc func open(_ sender: UITapGestureRecognizer) {
        let e: Custom = sender.view as! Custom
        let view: Card = Card()
      
        view.event = e.event
        present(view, animated: false, completion: nil)
    }
    
    /*
    @objc func scroll1(_ sender: UITapGestureRecognizer) {
        carusel.setContentOffset(CGPoint(x:self.view.frame.width, y:0), animated: true)
    }
    */
    
    var CHECKSUM = String()
    
    @objc func load() {
        DispatchQueue(label: "ru.miloconcerthall.label").async {
            do {
                let old = self.CHECKSUM
                if let url1 = URL(string: "http://miloconcerthall.ru/app/update") {
                    self.CHECKSUM = try String(contentsOf: url1)
                    print("CHECKSUM \(self.CHECKSUM)")
                }
                
                if (old != self.CHECKSUM) {
                    
                    let json = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("events.json")
                    
                    if let url2 = URL(string: "http://miloconcerthall.ru/app/json/events.json") {
                        print("update events feed")
                        try Data(contentsOf: url2).write(to: json)
                    }
                    
                    self.feed = try JSONDecoder().decode(Events.self, from: Data(contentsOf: json))
                    let size = self.feed.events.count
                    
                    for a in 0..<size {
                        let file = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(self.feed.events[a].intro)
                        
                        if FileManager.default.fileExists(atPath: file.path) {
                            print("exist \(self.feed.events[a].intro)")
                        } else {
                            if let url3 = URL(string: "http://miloconcerthall.ru/app/\(self.feed.events[a].intro)") {
                                print("load \(self.feed.events[a].intro)")
                                let image = UIImage(data: try Data(contentsOf: url3))
                                try image?.pngData()?.write(to: file)
                            }
                        }
                    }
                
                    DispatchQueue.main.async {
                        self.content()
                    }
                }
                
                DispatchQueue.main.async {
                    self.refresh.endRefreshing()
                }
            } catch let error {
                print(error)
                
                DispatchQueue.main.async {
                    self.refresh.endRefreshing()
                }
            }
        }
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle { return .lightContent }
}
